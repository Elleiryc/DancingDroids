use std::io;
extern crate rand;
use rand::Rng;

///  [Sructure] robot avec son nom ,sa direction et sa position.
struct Robot {
    nom: String,
    d: Direction,
    p: (i32,i32),
}

///  [Enum] avec orientations possibles du robot : North South East West
#[derive(Debug, PartialEq)]
enum Direction {
    North,//  North
    South,//  South
    East,//  East
    West,//  Ouest
}

///  [Enum] avec les différentes insctruction permettant de tourner le robot et de le faire avancer.
#[derive(Debug, PartialEq)] // affichage de debug ;)
enum Movement {
    L,//  left
    R,//  right
    F,//  Forward
}/// 

///  [Fonction] pour changer de direction courante en fonction du mouvement
///  faut s'imaginer que le robot tourne sur lui même
///  par exemple si je tourne à droite alors que mon robot regarde vers le Nord,
///  alors mon robot regarde vers l'Est, si je retourne à droite mon robot regarde vers le Sud.
///  Le robot avance dans le diretion qu'il regarde si sa direction actuelle est nord il avancera vers le nord.

fn forward(dir: &Direction, current_pos: (i32, i32)) -> (i32, i32) {
    use Direction as D;
    let (mut x, mut y) = current_pos;
    match dir {  // Avance dans la direction où regarde le robot
        D::North => y -=1,
        D::East => x +=1,
        D::South => y +=1,
        D::West => x -=1,
    }
    (x, y)
}

///  [Fonction] qui Change la direction du robot quand on tourne le robot à gauche North devient West, West devient South etc...
fn rotate_left(d: Direction) -> Direction {
    use Direction as D;
    match d {
        D::North => D::West, // Nord => Ouest
        D::East => D::North, // Est => Nord
        D::South => D::East, // Sud => Est
        D::West => D::South, // Ouest => Sud
    }
}

///  [Fonction] qui Change la direction du robot quand on tourne le robot à droite North devient East, East devient South etc...
#[warn(unused_must_use)]
fn rotate_right(d: Direction) -> Direction {
    use Direction as D;
    match d {
        D::North => D::East, // Nord => Est
        D::East => D::South, // Est => Sud
        D::South => D::West, // Sud => Ouest
        D::West => D::North, // Ouest => Nord
    }
}

///  [Fonction] qui permet à l'utilisateur d'entrer des caractères.
fn recuperer_entree_utilisateur() -> std::string::String {
    
    let mut entree = String::new();

    io::stdin().read_line(&mut entree); // on récupère ce qu'a entré l'utilisateur dans la variable entree

    entree
}

///  [Fonction] qui délimite le terrain.
///  On interdit au robot d'aller au-delà de x < 0 et x > 10,  y < 0 et y > 10,
///  On enferme le robot dans un terrain de (0,0) à (10,10).
fn limite_terrain(pos: (i32, i32),t: (i32,i32)) -> bool {
    !( (pos.0 < 0 || pos.0 > t.0) 
    || (pos.1 < 0 || pos.1 > t.1))
}

/// [Fonction] qui gère les collisions
fn collisions (p1: (i32, i32), p2: (i32, i32)) -> bool {

    !((p1.0 == p2.0) && (p1.1 == p2.1))

}

///  [Fonction] qui determine l'action faite grace au caractère entré par l'utilisateur.
///  F pour forward (avancer), R pour Right (tourner à droite), L pour Left (tourner à gauche);
///  Dans cette fonction on utilise vecteur v qui sert d'intermédiaire entre le saisie de
///  l'utilisateur(String) et les instruction(Enum Movement).

///  [Fonction] permettant de changer la position du robot grace au movement mov, on a besoin 
///  des coordonnées du terrain personnalisé pour les limittes du terrain.
///  L'inconveniant de cette fonction, c'est qu'on ne peut jouer qu'avec deux robot..
///  En fait on aurait pu jouer avec plus que deux robot en mettant les coordonnée de tout les robot
///  dans une fonction similaire à celle d'obstacle, mais on sait pas comment le faire.
fn movnt(mov: Movement,rb1: Robot,rb2: &Robot,t: (i32,i32)) -> Robot {

    let nom1 = rb1.nom;
    let d1 = rb1.d;
    let p1 = rb1.p;


    let mut r = Robot {
        nom: nom1,
        d: d1,
        p: p1,
    };

    let p2 = rb2.p;

    let r2 = Robot {
        nom: "lepape".to_string(),
        d: Direction::North,
        p: p2,
    };

    match mov {
        Movement::F =>{
            let test_pos = forward(&r.d,r.p);
                if movnt_bool(test_pos,t,&r, &r2)
                {
                    r.p=test_pos;
                }
            }
        Movement::R => r.d = rotate_right(r.d),
        Movement::L => r.d = rotate_left(r.d),
    }
    r
}
///  Fonction qui renvoie un boolean si le prochain forward est bloqué par :
///  un robot, la limite du terrain ou un obstacle.
fn movnt_bool(test_pos: (i32,i32),t: (i32,i32),r: &Robot,r2: &Robot ) -> bool {

        if limite_terrain(test_pos,t) && obstacle(test_pos) {

                if collisions(test_pos,r2.p) // on test si le robot ne va pas dans des direction interdite
                {
                    true
                }
                else {
                    print!("\n Collision en {:?} pour {:?}",r.p,r.nom);
                    false
                }
            }
            else {false}

}

///  [Fonction] qui permet d'afficher un robot sur un plateau aux bonnes coordonnées x et y.
fn test_coordonnee_pour_affichage (x: i32, y: i32, p1: (i32,i32)) -> bool {

    if p1.0 == x && p1.1 == y {true} else {false}

}

///     [Fonction] qui permet d'afficher les diretions des robot avec des fleches; par exemple north : ↑ 
fn affichage_direction_fleche(d: &Direction) {
    match d {
        Direction::North => print!(" ↑"),
        Direction::South => print!(" ↓"),
        Direction::East  => print!(" →"),
        Direction::West  => print!(" ←"),
    }
}

///  [Fonction] qui permet d'afficher dans le terminal le plateau de jeu, qui est une grille.
///  Avec les robot désigné par les caracatères O et K.
fn affichage_plateau(robot1: &Robot, robot2: &Robot, t: (i32,i32)) {
    
    print!("                          ");

    for jj in 0..=t.0 {
        if jj > 10 {print!("{} ", jj%jj+1);}
    }
    println!();
    print!("   ");

    for j in 0..=t.0 {
        if j > 10 {print!("{} ", j%10);}
        else {print!("{} ", j);}
    }
    println!();

    for i in 0..=t.1 {
        print!("{:2}", i);

        for k in 0..=t.0 {
            let tpl = (k,i);
            if test_coordonnee_pour_affichage(k,i,robot1.p) {affichage_direction_fleche(&robot1.d);}
            else if test_coordonnee_pour_affichage(k,i,robot2.p) {affichage_direction_fleche(&robot2.d);}
            else if !obstacle(tpl) {print!(" □");}
            else {print!(" _");}
        }
        print!("\n");
    }
}
///  Fonction qui donne les coordonnées des obctacles, 
///  qui return true si aucun robot ne va à ces coordonnées.
fn obstacle(tpl: (i32,i32)) -> bool {

   !((tpl.0 == 1 && tpl.1 == 5)
   || (tpl.0 == 5 && tpl.1 == 9)
   || (tpl.0 == 4 && tpl.1 == 5)
   || (tpl.0 == 9 && tpl.1 == 13)
   || (tpl.0 == 2 && tpl.1 == 10)
   || (tpl.0 == 7 && tpl.1 == 3)
   || (tpl.0 == 1 && tpl.1 == 5)
   || (tpl.0 == 2 && tpl.1 == 2)
   || (tpl.0 == 5 && tpl.1 == 5)
   || (tpl.0 == 7 && tpl.1 == 4)
   || (tpl.0 == 5 && tpl.1 == 13)
   || (tpl.0 == 9 && tpl.1 == 7)
   || (tpl.0 == 14 && tpl.1 == 4)
   || (tpl.0 == 13 && tpl.1 == 1)
   || (tpl.0 == 5 && tpl.1 == 6)
   || (tpl.0 == 7 && tpl.1 == 4)
   || (tpl.0 == 5 && tpl.1 == 11)
   || (tpl.0 == 9 && tpl.1 == 7)
   || (tpl.0 == 14 && tpl.1 == 2)
   || (tpl.0 == 13 && tpl.1 == 3)
   || (tpl.0 == 14 && tpl.1 == 4)
   || (tpl.0 == 13 && tpl.1 == 1)
   || (tpl.0 == 5 && tpl.1 == 6)
   || (tpl.0 == 7 && tpl.1 == 4)
   || (tpl.0 == 2 && tpl.1 == 11)
   || (tpl.0 == 6 && tpl.1 == 7)
   || (tpl.0 == 5 && tpl.1 == 2)
   || (tpl.0 == 3 && tpl.1 == 3)
   || (tpl.0 == 9 && tpl.1 == 4)
   || (tpl.0 == 11 && tpl.1 == 3)
   || (tpl.0 == 11 && tpl.1 == 7)
   || (tpl.0 == 8 && tpl.1 == 2)
   || (tpl.0 == 3 && tpl.1 == 10))


}


///  Fonction qui sert la fonction Choix_utilisateur, elle permet de prendre chaque str du vecteur intermédiaire
///  On utilise des str R et R\n, car l'action en fin de string a un \n.Movement
fn str_into_movement(input: &str) -> Movement {
    match input 
    {
        "F\n" => Movement::F,
        "L\n" => Movement::L,
        "R\n" => Movement::R,
        "F" => Movement::F,
        "L" => Movement::L,
        "R" => Movement::R,
        _ => Movement::F,
    }
}

/// Fonction qui sert à donner aleatoirement 10 instruction au robot,
fn aleatoire_vec() -> std::string::String {

    let mut string_aleatoire: std::string::String = String::from("");

    for x in 0..10 {
        let aleatoire = rand::thread_rng().gen_range(1, 100)%3;

        
        match aleatoire {
            0 => string_aleatoire.push('F'),
            1 => string_aleatoire.push('L'),
            2 => string_aleatoire.push('R'),
            _ => string_aleatoire.push('F'),
        }
        if x==9 {break;}
        string_aleatoire.push(' ');
    }
    string_aleatoire
}

/// [Fonction] qui permet de demande à l'utilisateur si il veut prendre des coordonnée de terrain
/// aléatoire ou pas, si il dit oui, on pren ddes coordonnée aléatoire entre 4 et 18, si non on prend
/// les coordonnées de bases ; 10 x 10.
fn coordonnee_terrain_perso() -> (i32,i32) {

    print!("Voulez vous une génération aléatoire du terrain ?('Oui' ou 'Non') \n(10x10 par défault)\n");

    let entree_utilisateur = recuperer_entree_utilisateur();
    let mut coord_perso: (i32,i32) = (0,0);

    if entree_utilisateur == "Oui\n" {

        let nb_x = 3 + rand::thread_rng().gen_range(1, 15);
        let nb_y = 3 + rand::thread_rng().gen_range(1, 15);
        coord_perso.0 = nb_x;
        coord_perso.1 = nb_y;

        print!("Vous avez choisi Oui.\nLe jeu se passera avec les coordonnée suivante :\n");
        print!("x : {:?}",coord_perso.0);
        print!("  y : {:?}\n",coord_perso.1);
    }
    else 
    {
        print!("Vous avez choisi Non.\nLe jeu se passera avec les coordonnée suivante :\n");
        coord_perso.0 = 10;
        coord_perso.1 = 10;

        print!("x : {:?}",coord_perso.0);
        print!("  y : {:?}\n",coord_perso.1);
    }
    coord_perso
}


///  [Fonction] principale
///  On créer deux robots, on affiche le terrain initial.
///  On leurs donne aléatoirement des instructions, on les fait bougé,
///  puis on affiche le terrain final.
fn main() {

    print!("\x1B[2J\x1B[1;1H");


    let coor_ter = coordonnee_terrain_perso();

    let x =coor_ter.0;
    let y =coor_ter.1;

    let mut robot_1 = Robot {
        nom: "Joseph".to_string(),
        d: Direction::South,
        p: (1,1),
    };


    let mut robot_2 = Robot {
        nom: "Robert".to_string(),
        d: Direction::North,
        p: (6%x,5%y),
    };

    print!("Etat initial : \n\n");

    affichage_plateau(&robot_1,&robot_2,coor_ter);

    let robot_1_s = aleatoire_vec();
    let robot_2_s = aleatoire_vec();


    let v1: Vec<&str> = robot_1_s.split(' ').collect();
    let v2: Vec<&str> = robot_2_s.split(' ').collect();

    let mut strvar: &str;
    let mut mov: Movement;


    for x in 0..v1.len() {

        strvar = v1[x];
        mov= str_into_movement(strvar);
        robot_1 = movnt(mov,robot_1,&robot_2,coor_ter);

        strvar = v2[x];
        mov= str_into_movement(strvar);
        robot_2 = movnt(mov,robot_2,&robot_1,coor_ter);
        
    }

    print!("\nInstruction aléatoire de {:?} : {:?}",robot_1.nom,robot_1_s);

    print!("\nInstruction aléatoire de {:?} : {:?}\n\n",robot_2.nom,robot_2_s);
        
    print!("\n");
    println!("Etat Final : \n\n");

    affichage_plateau(&robot_1,&robot_2,coor_ter);


}

/// Tests
#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_rotate_left() {
        let dir = Direction::North;
        assert_eq!(Direction::West, rotate_left(dir));
    }

    #[test]
    fn test_rotate_right() {
        let dir = Direction::North;
        assert_eq!(Direction::East, rotate_right(dir));
    }

    #[test]
    fn tets_forward()
    {
        let pos_robot: (i32,i32) = (1,1);
        let dir_robot = Direction::North;
        assert_eq!((1,0), forward(&dir_robot, pos_robot));
    }

    #[test]
    fn test_str_into_movement() {
        let _str: &str ="R";
        assert_eq!(Movement::R,str_into_movement(_str));

        let _str: &str ="L";
        assert_eq!(Movement::L,str_into_movement(_str));

        let _str: &str ="F";
        assert_eq!(Movement::F,str_into_movement(_str));

        let _str: &str ="R\n";
        assert_eq!(Movement::R,str_into_movement(_str));
    }

    #[test]
    fn test_collisions() {
        let mut pos_robot1 = (1,1);
        let mut pos_robot2 = (1,1);

        assert_eq!(false,collisions(pos_robot1,pos_robot2));

        pos_robot1 = (1,1);
        pos_robot2 = (8,19);

        assert_eq!(true,collisions(pos_robot1,pos_robot2));
    }

    #[test]
    fn test_affichage_direction_fleche() {
        let dir = Direction::North;
        assert_eq!(print!(" ↑"),affichage_direction_fleche(&dir));
    }

    #[test]
    fn test_limite_terrain() {
        let mut pos_robot: (i32,i32) = (1,1);
        let pos_terrain: (i32,i32) = (5,5);

        assert_eq!(true,limite_terrain(pos_robot, pos_terrain));

        pos_robot = (7,7);

        assert_eq!(false,limite_terrain(pos_robot, pos_terrain));
    }

    #[test]

    fn test_recuperer_entree_utilisateur() {

        //Entrer le mot 'test' pour tester
        let mut truc: std::string::String = String::from("test\n");
        assert_eq!(truc,recuperer_entree_utilisateur());

        //Entrer 'F R L' pour tester
        truc = String::from("F R L\n");
        assert_eq!(truc,recuperer_entree_utilisateur());


    }

    #[test]
    fn test_movnt_bool() {
  

        let r = Robot {
            nom: String::from("Christophe Maé"),
            d: Direction::South,
            p: (0,0),
        };

        let mut r2 = Robot {
            nom: String::from("Christophe Colomb"),
            d: Direction::East,
            p: (2,3),
        };

        let test_pos = forward(&r.d,r.p);

        let t = (10,10);
        
        assert_eq!(true,movnt_bool(test_pos, t, &r, &r2));

        r2.p = (0,1);
        assert_eq!(false,movnt_bool(test_pos, t, &r, &r2));
    }

    #[test]
    fn test_obstacle() {
        
        let mut pos_robot = (5,9);
        assert_eq!(false,obstacle(pos_robot));

        pos_robot = (1,1);
        assert_eq!(true,obstacle(pos_robot));

    }
}   